var mongoose = require('mongoose');

var dbURI = 'mongodb://localhost/test';

mongoose.connect(dbURI);

mongoose.connection.on('connected', function () {
  console.log('Mongoose default connection open to ' + dbURI);
});


mongoose.connection.on('error',function (err) {
  console.log('Mongoose default connection error: ' + err);
});


mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});
