const express = require('express');
const multer = require('multer');
const fileExtension = require('file-extension');
const sizeOf = require('image-size');
const app = express();

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Methods', 'POST, PUT, OPTIONS, DELETE, GET');
  res.header('Access-Control-Allow-Origin', null);
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

const db = require('./model/db');
const image = require('./model/images');

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, './uploads');
  },
  filename: (req, file, callback) => {
    const id = Date.now();
    const ext = fileExtension(file.originalname);
    callback(null, `${file.fieldname}-${id}.${ext}`);
  }
});

const upload = multer({ storage }).single('img');

app.post('/images', (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      res.end('Error uploading file.');
    }
    const dimensions = sizeOf(req.file.path);
    res.send({
      res: 'Image uploaded!',
      mime: req.file.mimetype,
      path: req.file.path,
      width: dimensions.width,
      height: dimensions.height,
      size: req.file.size
    });
    res.end();
  });
});

app.listen(3000, () => {
  console.log('Working on port 3000');
});
